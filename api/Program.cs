using System.IO;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace api;

public class Program
{
    public static void Main(string[] args)
    {
        CreateHost(args).Build().Run();
    }



    public static IHostBuilder CreateHost(string[] args)
    {
        return Host
            .CreateDefaultBuilder(args)
            .UseServiceProviderFactory(new AutofacServiceProviderFactory())
            .ConfigureAppConfiguration(x =>
                                       {
                                           x.SetBasePath(Directory.GetCurrentDirectory())
                                               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                                               .AddEnvironmentVariables()
                                               .AddCommandLine(args);
                                       })
            .ConfigureWebHostDefaults(webBuilder =>
                                  {
                                      webBuilder.UseStartup<Startup>();
                                      if (System.Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
                                          webBuilder.UseUrls("http://*:5000;https://*:5001");
                                      else
                                          webBuilder.UseUrls("http://*");
                                  });
    }
}
