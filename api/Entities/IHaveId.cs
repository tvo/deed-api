namespace api.Entities;

public interface IHaveId
{
    int Id { get; }
}
