using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers;

using api.db;
using core.logging;

public class CrudOperations<Entity>
    where Entity : class, api.Entities.IHaveId
{
    private readonly DataContext context;
    private readonly ILoggerAdapter logger;
    private DbSet<Entity> data => context.Set<Entity>();

    public CrudOperations(DataContext context, ILoggerAdapter<CrudOperations<Entity>> logger)
    {
        this.context = context;
        this.logger = logger;
    }

    public IQueryable<Entity> GetMany() => data.AsNoTracking().OrderBy(x => x.Id);
    public async Task<Entity> GetSingle(int id) => await data.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);

    public async Task<Entity> Post(Entity entity)
    {
        data.Add(entity);
        await context.SaveChangesAsync();
        return entity;
    }

    public async Task<Entity> Delete(int id)
    {
        logger.LogWarning("Deleting {EntityName}: {Id}", typeof(Entity).Name, id);
        var entity = await data.FirstOrDefaultAsync(x => x.Id == id);
        data.Remove(entity);
        await context.SaveChangesAsync();
        return entity;
    }

    public async Task<ActionResult<List<Entity>>> PutMany(List<Entity> entities)
    {
        foreach (var entity in entities)
        {
            if (!await data.AnyAsync(x => x.Id == entity.Id))
                return new NotFoundObjectResult(entity);
            context.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }
        await context.SaveChangesAsync();
        return entities;
    }

    public async Task<ActionResult<Entity>> Put(Entity entity)
    {
        logger.LogWarning(entity.Id.ToString());
        if (!await data.AnyAsync(x => x.Id == entity.Id))
            return new NotFoundObjectResult(entity);
        context.Attach(entity);
        context.Entry(entity).State = EntityState.Modified;
        await context.SaveChangesAsync();
        return entity;
    }
}
