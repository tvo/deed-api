using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;

namespace api.Controllers;

using db;
using Entity = api.Entities.Deed;

[ApiController]
[Route("[controller]")]
public class DeedController : ControllerBase
{
    private readonly CrudOperations<Entity> crud;
    private readonly DbSet<Entity> data;
    private readonly ILogger logger;
    private readonly DataContext context;

    public DeedController(CrudOperations<Entity> crud, DataContext context, ILogger<DeedController> logger)
    {
        this.crud = crud;
        this.context = context;
        this.data = context.Set<Entity>();
        this.logger = logger;
    }

    [HttpGet]
    public IEnumerable<Entity> GetMany() => crud.GetMany(); // todo filter based on your tenant path.

    [HttpGet("{id}")]
    public Task<Entity> GetSingle([FromRoute] int id) => crud.GetSingle(id); // todo add filtering.

    public static Regex PathRegex = new Regex("""^(\/[0-9a-zA-Z]+)+$""", RegexOptions.Compiled & RegexOptions.Singleline);

    private async Task<bool> HasPermission(string path)
    {
        return true;
    }

    private async Task<(bool error, string text)> ValidateEntity(Entity entity, bool isAdminRole, Entity? oldEntity = null)
    {
        if(string.IsNullOrWhiteSpace(entity.Path))
            return (true, "Path alphanumeric.");
        if(!PathRegex.IsMatch(entity.Path))
            return (true, "Path is invalid.");
        // Direct duplicate.
        if(entity.Id == 0 && await this.data.Where(x => x.Path == entity.Path && (x.UserId == 0 || x.UserId == entity.UserId)).AnyAsync())
            return (true, $"Duplicate path {entity.Path} for userId {entity.UserId}");

        // todo: check permissions here.
        // todo: I can't add to a deed that I don't own.
        // todo: check that entity.UserId is same as my id - if not admin.

        var isRoot = entity.Path.Count(x => x == '/') == 1;
        if(!isRoot)
        {
            var parentPath = entity.Path.Substring(0, entity.Path.LastIndexOf('/'));
            logger.LogInformation("parent path::: {ParentPath}", parentPath);
            if (!await this.data.AnyAsync(x => x.Path == parentPath && (x.UserId == 0 || x.UserId == entity.UserId)))
                return (true, "Could not find a parent deed given the path.");
            if (!await HasPermission(parentPath))
                return (true, "Do not have permission for desired path.");
        }
        else // is root
        {
            // check if you are the admin.
        }

        if(oldEntity != null)
        {
            // update validation
            // todo: check for a move. Specifically if you have permissions for the origional.
            if (!await HasPermission(oldEntity.Path))
                return (true, "Do not have permission for desired path.");
        }

        return (false, string.Empty);
    }

    [HttpPost]
    public async Task<ActionResult<Entity>> Post([FromBody] Entity entity)
    {
        entity.Id = 0;
        var validation = await ValidateEntity(entity, true);
        if(validation.error)
            return BadRequest(validation.text);
        this.data.Add(entity);
        await context.SaveChangesAsync();
        return Ok(entity);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<Entity>> Delete(int id)
    {
        var old = await data.FirstOrDefaultAsync(x => x.Id == id);
        if (old is null || !await HasPermission(old.Path))
            return BadRequest("Could not find entity.");
        context.Remove(old);
        await context.SaveChangesAsync();
        return Ok();
    }

    [HttpPut]
    public async Task<ActionResult<Entity>> Put([FromBody] Entity entity)
    {
        if(entity.Id == 0)
            return await Post(entity);
        var old = data.FirstOrDefaultAsync(x => x.Id == entity.Id);
        if(old == null)
            return BadRequest("Could not find entity.");

        var validation = await ValidateEntity(entity, true);
        if(validation.error)
            return BadRequest(validation.text);

        data.Attach(entity);
        await context.SaveChangesAsync();
        return Ok(entity);
    }
}
