using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Autofac;
using Microsoft.EntityFrameworkCore;

namespace api;

using db;
using core.logging;
using core.webSockets;

public class Startup
{
    public const string ApiPrefix = "api";

    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        var sql = Configuration["sql"];
        services.AddDbContext<db.DataContext>(options => options.UseNpgsql(sql));
        services.AddControllers();
        services.AddSwaggerGen(c =>
           {
               c.SwaggerDoc("v1", new OpenApiInfo { Title = "api", Version = "v1" });
           });
        services.AddHostedService<DbService>();
    }

    public void ConfigureContainer(Autofac.ContainerBuilder builder)
    {
        builder.RegisterGeneric(typeof(Controllers.CrudOperations<>));
        builder.RegisterType<DbService>();
        builder.RegisterModule(new LoggerModule(Configuration));
        builder.RegisterModule(new WebSocketModule(Configuration));
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
            app.UseDeveloperExceptionPage();

        app.UseSwagger(c => c.RouteTemplate = ApiPrefix + "/swagger/{documentname}/swagger.json");
        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint($"/{ApiPrefix}/swagger/v1/swagger.json", "api v1");
            c.RoutePrefix = $"{ApiPrefix}/swagger";
        });
        app.UsePathBase($"/{ApiPrefix}");

        //app.UseHttpsRedirection();
        app.UseRouting();
        app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
        //app.UseAuthorization();
        app.UseEndpoints(endpoints =>
             {
                 endpoints.MapControllers();
             });
    }
}
