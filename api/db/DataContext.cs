using api.Entities;
using Microsoft.EntityFrameworkCore;

namespace api.db;


public class DataContext : DbContext
{
    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<Deed>();
    }

    public DbSet<Deed> Deed { get; set; }
}

