ARG BASE_ARCH=mcr.microsoft.com/dotnet/sdk:7.0
ARG RUN_ARCH=mcr.microsoft.com/dotnet/aspnet:7.0

FROM ${BASE_ARCH} AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY api/*.csproj ./api/
COPY tests/*.csproj ./tests/
COPY *.sln ./
COPY NuGet.config ./
RUN dotnet restore

# Copy everything else and build
COPY ./api ./api
RUN dotnet publish api -c Release -o out

# Build runtime image
FROM ${RUN_ARCH}
EXPOSE 3000/tcp
WORKDIR /api
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "api.dll"]
