docker stop pg-docker-deed || true
docker rmi pg-docker-deed || true

sleep 1

docker run -d --name pg-docker-deed \
	-p 5433:5432 \
	-e POSTGRES_PASSWORD=docker \
	-v pgdata:/var/lib/postgresql/data \
	postgres || docker start pg-docker-deed

